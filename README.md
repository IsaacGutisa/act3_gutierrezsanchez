

/**
 *
* @author Gutiérrez Sánchez Isaac
 */

import java.util.Arrays;
import java.util.Scanner;

public class Buscador {
//git clone https://IsaacGutisa@bitbucket.org/IsaacGutisa/act3_gutierrezsanchez.git

public static int hacerBusquedaBinaria(Comparable[] array, Comparable value) {
        int primerelementodelarreglo,ultimoelementodelarreglo ; // Priemr y ultimo elemento del arreglo   
        int puntomedio, posicionidicadora; // punto medio y posición que dirá dónde se halló
        boolean localizado; 

    // Establezcamos lo logico para poder iniciar
        primerelementodelarreglo = 0; //iniciamos de 0 en adelante
        ultimoelementodelarreglo = array.length - 1; //evitemos salir del rango del arreglo
        posicionidicadora = -1; //no entramos aun al arreglo
        localizado = false; //de entrada no hemos hechio nada

// Busquemos
    while (!localizado && primerelementodelarreglo <= ultimoelementodelarreglo) {
        // Media aritmetcia para punto medio
        puntomedio = (primerelementodelarreglo + ultimoelementodelarreglo) / 2;

        // Si pasa que el objeto coincide con estar en el ue resultó ser el punto medio
            if (array[puntomedio].compareTo(value) == 0) {
                localizado = true;
                posicionidicadora = puntomedio;
            }
        // si está por debajo de la media, buscamos ahí
        else if (array[puntomedio].compareTo(value) > 0)
            ultimoelementodelarreglo = puntomedio - 1;
        // si está por arriba de la media, buscamos ahí
        else
            primerelementodelarreglo = puntomedio + 1;
    }
return posicionidicadora; 
}//si ni en la media, ni antes ni despupés se encontró se devuelve intacto la posicion indicadora que era -1

    public static void main(String[] args) {
        //////////////////////////INICIO BUSQUEDA BINARIA
        int puntodelResultado;
        String valoraBuscar;
        
        String[] values = {"jakin", "schiboleth" , "tubalcain", "mass"}; //los valores arbitrarios
      
        Scanner sc = new Scanner(System.in); // ingrese el que desee buscar

        Arrays.sort(values);

//         Muestra el conjunto de Datos
            System.out.println("El conjuntio de datos es: ");
            for (String element : values)
            System.out.print(element + "   "); System.out.println();

        
            // Obteenr un valor a buscar
            System.out.print("Ingrese el valor que desea buscar ");
            valoraBuscar = sc.nextLine();

            // Buscar lo ingresado
            puntodelResultado = Buscador.hacerBusquedaBinaria(values, valoraBuscar);

            // Muestra el resultado final
            if (puntodelResultado == -1)
                System.out.println(valoraBuscar + " no está en el conjunto de valores");
            else {
                System.out.println(valoraBuscar + " se localiza en el lugar " + puntodelResultado);
            }
/////////////////////////////FIN  BUSQUEDA BINARIA
          
    }
}
